import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class NumberOfCharacters extends JFrame implements ActionListener {
    JTextField tf1;
    JTextField tf2;
    JButton b;
    NumberOfCharacters(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        tf1= new JTextField("testfile.txt"); //
        tf1.setBounds(100 , 10, 200,40);

        tf2= new JTextField();
        tf2.setBounds(100 , 60, 200,40);

        b=new JButton();
        b.setBounds(170,120,60,30);
        b.addActionListener(this);

        add(tf1);add(tf2);add(b);
        setResizable(false);
        setLayout(null);
        setSize(400,400);
        setVisible(true);
    }

    public void actionPerformed (ActionEvent e){
        if(e.getSource()==b)
        {
            int count=0;
            try {
                FileReader reader=new FileReader("src/s2/"+tf1.getText());

                while(reader.read()!=-1){
                    count++;
                }
                reader.close();
                tf2.setText(String.valueOf(count));
            } catch (FileNotFoundException fileNotFoundException) {
                tf2.setText("File not found!");
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {

        new NumberOfCharacters();
    }

}
